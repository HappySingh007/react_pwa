import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Card extends Component {
    render() {
        return (
            <div>
            <div >
              <img src={this.props.Repo.owner.avatar_url} style={{height:100 + 'px',width:'100px', 'borderRadius':'50%'}} />
              {this.props.Repo.description}
            </div>
            <div >
              <a >
                {this.props.Repo.stargazers_count} Stars
              </a>
            </div>
          </div>
        );
    }
}

Card.propTypes = {
    Repo : PropTypes.object.isRequired
};

export default Card;