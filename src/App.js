import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import Card from './card';

class App extends Component {
  async componentDidMount() {
    let result = await fetch("https://api.github.com/users/google/repos");

    let json = await result.json();
    this.setState({
      repos: json,
      isloading: false
    });
  }
  constructor(props) {
    super(props);
    this.state = {
      isloading: true
    };
  }
  render() {
    let load = this.state.isloading;
    let repository = this.state.repos;
    return (
      <div>
        {!load &&
          repository.map(repo =>
            <div key={repo.id}>
              <Card Repo={repo} />
            </div>
          )}
      </div>
    );
  }
}
export default App;
